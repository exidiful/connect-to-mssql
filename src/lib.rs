#![allow(dead_code)]
use anyhow::Ok;
use async_std::net::TcpStream;
use once_cell::sync::Lazy;
use std::env;
use tiberius::{Client, Config};

static JDBC_CONN_STR_PORT: Lazy<String> = Lazy::new(|| {
    env::var("TIBERIUS_TEST_CONNECTION_STRING_WITH_PORT").unwrap_or_else(|_| {
        "jdbc:sqlserver://127.0.0.1:1433;database=customers;user=sa;password=$Hark2010;trustServerCertificate=true".to_owned()
    })
});

// Connect to a named SQL Server instance using a JDBC connection string
async fn connect_with_jdbc_connection_string_port() -> anyhow::Result<()> {
    let config = Config::from_jdbc_string(&JDBC_CONN_STR_PORT)?;

    let tcp = TcpStream::connect(config.get_addr()).await?;
    tcp.set_nodelay(true)?;

    let client = Client::connect(config, tcp).await?;
    println!("Successfully connected to server.");

    // And then close the connection.
    client.close().await?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[async_std::test]
    async fn test_connect_through_port() {
        let result = connect_with_jdbc_connection_string_port().await;
        assert_eq!(result.is_ok(), true);
    }
}
